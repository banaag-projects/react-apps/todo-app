import { Col, Container, Row, Form } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { todoActions } from "../reducers/todoSlice";
import CheckCircleIcon from "@mui/icons-material/CheckCircle";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";
import DoneIcon from "@mui/icons-material/Done";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import ExpandLessIcon from "@mui/icons-material/ExpandLess";
import { useState } from "react";

function TodoList() {
  const dispatch = useDispatch();
  const todos = useSelector((state) => state.todosReducer.todosArray);

  const [updatedTodo, setUpdatedTodo] = useState("");
  const [comment, setComment] = useState("");

  //FUNCTIONS

  function markComplete(itemId, value) {
    console.log(itemId);
    console.log(value);
    dispatch(
      todoActions.markAsComplete({ id: itemId, isCompleteValue: value })
    );
  }

  function handleEditButton(itemId, value) {
    dispatch(
      todoActions.handleToBeUpdated({ id: itemId, showUpdateValue: value })
    );
  }

  function handleEditDoneButton(itemId) {
    dispatch(
      todoActions.handleToBeUpdatedDone({ id: itemId, editedName: updatedTodo })
    );
    setUpdatedTodo("");
  }

  function removeTodo(itemId) {
    dispatch(todoActions.removeTodo(itemId));
  }

  function showComment(itemId, value) {
    console.log(itemId);
    console.log(value);

    dispatch(
      todoActions.showComment({
        showCommentState: value,
        id: itemId,
      })
    );
  }

  function editComment(e, itemId) {
    setComment(e);
    dispatch(todoActions.editComment({ editedComment: comment, id: itemId }));
  }

  return (
    <Container
      fluid
      style={{
        border: "2px solid #8c92ac",
        borderLeft: "7px solid #0079bf",
        borderTopLeftRadius: "10px",
        borderBottomLeftRadius: "10px",
        marginBottom: "20px",
        padding: "20px",
      }}
    >
      {todos.map((item) => (
        <Row
          key={item.id}
          className="mt-2"
          style={{
            border: "2px solid #ccc",
            padding: "20px",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          {item.isComplete ? (
            <Col lg={1}>
              <CheckCircleIcon
                color="primary"
                onClick={() => markComplete(item.id, item.isComplete)}
              />
            </Col>
          ) : (
            <Col lg={1}>
              <CheckCircleIcon
                onClick={() => markComplete(item.id, item.isComplete)}
              />
            </Col>
          )}
          {item.toBeUpdated ? (
            <Col lg={9}>
              <Form.Control
                placeholder={item.todoName}
                aria-label="todo name"
                aria-describedby="basic-addon1"
                value={updatedTodo}
                onChange={(e) => setUpdatedTodo(e.target.value)}
              />
            </Col>
          ) : (
            <Col lg={9}>{item.todoName}</Col>
          )}

          {item.toBeUpdated ? (
            <Col lg={1}>
              <DoneIcon onClick={() => handleEditDoneButton(item.id)} />
            </Col>
          ) : (
            <Col lg={1}>
              <EditIcon
                onClick={() => handleEditButton(item.id, item.toBeUpdated)}
              />
            </Col>
          )}

          <Col lg={1}>
            <DeleteIcon onClick={() => removeTodo(item.id)} />
          </Col>

          <Col lg={2} className="p-0" style={{ alignItems: "center" }}>
            {item.isCommentShown ? (
              <Row
                className="p-0 m-0"
                style={{ fontSize: 10 }}
                onClick={() => showComment(item.id, item.isCommentShown)}
              >
                Hide Comment
              </Row>
            ) : (
              <Row
                className="p-0 m-0"
                style={{ fontSize: 10 }}
                onClick={() => showComment(item.id, item.isCommentShown)}
              >
                Show Comment
              </Row>
            )}
          </Col>
          <Col
            lg={1}
            className="text-left p-0"
            style={{ alignItems: "center" }}
          >
            {item.isCommentShown ? (
              <ExpandLessIcon
                onClick={() => showComment(item.id, item.isCommentShown)}
              />
            ) : (
              <ExpandMoreIcon
                onClick={() => showComment(item.id, item.isCommentShown)}
              />
            )}
          </Col>

          {item.isCommentShown && (
            <Row className="mt-3">
              <Form.Control
                placeholder={item.comment}
                aria-label="todo comment"
                aria-describedby="basic-addon1"
                value={comment}
                onChange={(e) => editComment(e.target.value, item.id)}
              />
            </Row>
          )}
        </Row>
      ))}
    </Container>
  );
}

export default TodoList;
