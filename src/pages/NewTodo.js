import { useState } from "react";
import { Form, Button, Container, Row, Col } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { todoActions } from "../reducers/todoSlice";
import TodoList from "./TodoList";

function NewTodo() {
  const dispatch = useDispatch();
  const [todoName, setTodoName] = useState("");

  const todos = useSelector((state) => state.todosReducer.todosArray);
  console.log(todos);
  return (
    <Container
      style={{
        padding: "20px",
      }}
    >
      <Row
        style={{
          justifyContent: "center",
          alignItems: "center",
          height: "10vh",
        }}
      >
        <Col lg={6}>
          <Form
            style={{
              padding: "20px",
              border: "2px solid #8c92ac",
              borderTop: "10px solid #0079bf",
              borderTopLeftRadius: "10px",
              borderTopRightRadius: "10px",
            }}
          >
            <Form.Group className="mb-3">
              <Form.Control
                type="text"
                value={todoName}
                onChange={(e) => setTodoName(e.target.value)}
                placeholder="Add new todo"
              />
            </Form.Group>

            <Button
              variant="primary"
              style={{ width: "100%" }}
              onClick={() => {
                dispatch(
                  todoActions.createTodo({ name: todoName, id: Math.random() })
                );
              }}
            >
              ADD TODO
            </Button>
          </Form>
        </Col>
      </Row>
      <Row
        style={{
          justifyContent: "center",
          alignItems: "center",
          height: "10vh",
          marginTop: "55px",
        }}
      >
        <Col lg={6}>
          <TodoList />
        </Col>
      </Row>
    </Container>
  );
}

export default NewTodo;
