import { useState, useEffect } from "react";
import { Container, Form, Button, Row, Col } from "react-bootstrap";
import { Navigate } from "react-router-dom";

function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isActive, setIsActive] = useState(false);
  const user = localStorage.getItem("email");

  function login() {
    localStorage.setItem("email", email);
  }

  useEffect(() => {
    if (email !== "" && password !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  return user !== null ? (
    <Navigate to="/todo" />
  ) : (
    <Container>
      <Row
        style={{
          justifyContent: "center",
          alignItems: "center",
          height: "93vh",
        }}
      >
        <Col lg={5}>
          <Form className="p-3" onSubmit={login}>
            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Control
                type="email"
                placeholder="Username"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-4" controlId="formBasicPassword">
              <Form.Control
                type="password"
                placeholder="Password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
            </Form.Group>

            {isActive ? (
              <Button variant="primary" type="submit" style={{ width: "100%" }}>
                Login
              </Button>
            ) : (
              <Button
                variant="primary"
                type="submit"
                disabled
                style={{ width: "100%" }}
              >
                Login
              </Button>
            )}
          </Form>
        </Col>
      </Row>
    </Container>
  );
}

export default Login;
