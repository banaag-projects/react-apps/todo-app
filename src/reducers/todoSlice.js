import { createSlice } from "@reduxjs/toolkit";

const todoSlice = createSlice({
  name: "todos",
  initialState: {
    todosArray: [],
  },
  reducers: {
    createTodo(state, action) {
      const newTodoName = action.payload.name;
      const keyId = action.payload.id;
      state.todosArray.push({
        todoName: newTodoName,
        isComplete: false,
        comment: "Comment",
        isCommentShown: false,
        toBeUpdated: false,
        id: keyId,
      });
    },
    markAsComplete(state, action) {
      const index = action.payload.id;
      const complete = action.payload.isCompleteValue;
      const item = state.todosArray.find((item) => item.id === index);
      item.isComplete = !complete;
    },

    handleToBeUpdated(state, action) {
      const index = action.payload.id;
      const showUpdate = action.payload.showUpdateValue;
      const item = state.todosArray.find((item) => item.id === index);
      item.toBeUpdated = !showUpdate;
    },

    handleToBeUpdatedDone(state, action) {
      const index = action.payload.id;
      const item = state.todosArray.find((item) => item.id === index);
      item.toBeUpdated = false;
      item.todoName = action.payload.editedName;
    },
    removeTodo(state, action) {
      const index = action.payload;
      state.todosArray = state.todosArray.filter((item) => item.id !== index);
    },

    showComment(state, action) {
      const index = action.payload.id;
      const showComment = action.payload.showCommentState;
      const item = state.todosArray.find((item) => item.id === index);
      item.isCommentShown = !showComment;
    },
    editComment(state, action) {
      const index = action.payload.id;
      const item = state.todosArray.find((item) => item.id === index);
      item.toBeUpdated = false;
      item.comment = action.payload.editedComment;
    },
  },
});

export const todoActions = todoSlice.actions;

export default todoSlice;
