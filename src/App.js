import { BrowserRouter } from "react-router-dom";
import { Route, Routes } from "react-router-dom";
import { Container } from "react-bootstrap";
import NewTodo from "./pages/NewTodo.js";
import Login from "./pages/Login.js";

function App() {
  return (
    <BrowserRouter>
      <Container fluid className="p-0 m-0">
        <Routes>
          <Route path="/" element={<Login />} />
          <Route path="/todo" element={<NewTodo />} />
        </Routes>
      </Container>
    </BrowserRouter>
  );
}

export default App;
